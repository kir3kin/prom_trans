/* menu bar */
$(document).ready(function () {
	/*
var win_w = $(window).width();
	if ( win_w >= 768 ) {
		$('#nav .drop').hover(function(e){
			e.preventDefault();
			if ($(this).find('.drop-menu').css('display') == 'none'){
				$(this).find('.drop-menu').slideDown(100);
				$(this).addClass('hover');
			}
			else {
				$(this).removeClass('hover');
				$(this).find('.drop-menu').slideUp(100);
			}
			
		})
	}
	else {
		$('#nav .drop').click(function(e){
			e.preventDefault();
			if ($(this).find('.drop-menu').css('display') == 'none'){
				$(this).find('.drop-menu').slideDown(100);
				$(this).addClass('hover');
			}
			else {
				$(this).removeClass('hover');
				$(this).find('.drop-menu').slideUp(100);
			}
			
		})
		$(document).mouseup(function (e){ 
		var div = $(".drop-menu"); 
			if (!div.is(e.target) 
			    && div.has(e.target).length === 0) { 
				div.slideUp();
			}
		});
	}
*/$('.order-link, .reg').click(function(e){
		e.preventDefault();
		if ($('.popup').css('display') == 'none'){
			$('.popup').center();
			$('.popup').fadeIn(200);
			$(this).addClass('active');
			$('body').prepend('<div class="shadow"></div>');
		}
		else {
			$(this).removeClass('active');
			$('.popup').fadeOut(100);
			$('.shadow').remove(200);
		}
		
	})
	/*Center block begin*/
		jQuery.fn.center = function () {
			this.css("top", (($(window).height() - this.outerHeight()) / 2) + $(window).scrollTop() + "px");
			this.css("left", (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft() + "px");
			return this;
		}
/*Center block end*/
	$(document).mouseup(function (e){ 
		var div = $(".popup"); 
			if (!div.is(e.target) 
			    && div.has(e.target).length === 0) { 
				div.fadeOut(100);
				$('.shadow').remove();
			}
		});
$('.menu-phone').click(function(e){
		e.preventDefault();
		if ($('#nav').css('display') == 'none'){
			$('#nav').slideDown();
			$(this).addClass('active');
			$('#nav').addClass('mob-nav');
			$('body').prepend('<div class="shadow-menu"></div>');
		}
		else {
			$(this).removeClass('active');
			$('#nav').removeClass('mob-nav');
			$('#nav').slideUp();
			$('.shadow-menu').remove();
		}
		
	})
	
});
// Top menu

jQuery(document).ready(function(){

$('.menu > ul > li ').each ( function(ind,el){
	var el_width=$(el).outerWidth(); // total width of li
	var max_w=0; // max width
	var max_padd=0; // max padding
	var i=0, j=0;
	
	var ul=$(el).find('ul').css( {"visibility": "hidden", "display": "block"});
	i= (($(el).outerHeight())+0)+'px';
	
	var child = $(el).find('ul > li > a');
	child.each(function(j, li) {
		i= $(li).outerWidth(); // total width
		j= i-$(li).width(); // padding
		max_w = i > max_w ? i : max_w;
		max_padd = j > max_padd ? j : max_padd;
	    }); // each child
	
	
	max_w= ((el_width - max_padd) > max_w)? el_width - max_padd +10 :max_w; // if <a> narrowest then parent <li> 
	$(ul).width(max_w+max_padd);

	$(el).find('ul').find('a').width(max_w);

	$(ul).css( {"visibility": "", "display": "none"});
	
})

var was_click=false; // for mobile version. if we are able to click without having to call hover this is mobile version
var was_hover=false; // if hover true this is non mobile version
var last_element=$(); // last clicked element;

$('.menu > ul > li ').hover ( 
	function (){
		if (was_click) {return;}
		was_hover=true;
		var child = $(this).find('ul');
		last_element=child;
		if ( $(child).is(':animated')) { 
				$(child).stop(false,false); 
		}
		$(child).animate( {'height':'toggle'},100);
		return false;
})	
$('.menu > ul > li ').click ( 
	function (){
		if (was_hover) {return;}
		was_click=true;
		if (last_element) {
			if ( $(last_element).is(':animated')) { 
				$(last_element).stop(false,false); 
			}
			$(last_element).animate( {'height':'toggle'},100);
		}
		var child = $(this).find('ul');
		last_element=child;
		if ( $(child).is(':animated')) { 
				$(child).stop(false,false); 
		}
		$(child).animate( {'height':'toggle'},100);
		return false;
})// topmenu	

});