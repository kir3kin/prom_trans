<?php //foreach ($modules as $module) { ?>
<?php //echo $module; ?>
<?php //} ?>
			<div id="content">
				<!-- Start WOWSlider.com BODY section --> <!-- add to the <body> of your page -->
				<div id="wowslider-container1">
					<div class="ws_images">
						<ul>
							<li><img src="catalog/view/theme/prom_trans/images/img-01.jpg" alt="img-01" title="img-01" id="wows1_0"/></li>
							<li><img src="catalog/view/theme/prom_trans/images/img-02.jpg" alt="img-02" title="img-02" id="wows1_1"/></li>
							<li><img src="catalog/view/theme/prom_trans/images/img-03.jpg" alt="jquery slideshow" title="img-03" id="wows1_2"/></a></li>
							<li><img src="catalog/view/theme/prom_trans/images/img-04.jpg" alt="img-04" title="img-04" id="wows1_3"/></li>
						</ul>
					</div>
					<div class="ws_bullets">
						<div>
							<a href="#" title="img-01"><span><img src="catalog/view/theme/prom_trans/images/img01-min.png" alt="img-01"/>1</span></a>
							<a href="#" title="img-02"><span><img src="catalog/view/theme/prom_trans/images/img02-min.png" alt="img-02"/>2</span></a>
							<a href="#" title="img-03"><span><img src="catalog/view/theme/prom_trans/images/img03-min.png" alt="img-03"/>3</span></a>
							<a href="#" title="img-04"><span><img src="catalog/view/theme/prom_trans/images/img04-min.png" alt="img-04"/>4</span></a>
						</div>
					</div>
					<div class="ws_shadow"></div>
				</div>	
				<script type="text/javascript" src="catalog/view/theme/prom_trans/js/wowslider.js"></script>
				<script type="text/javascript" src="catalog/view/theme/prom_trans/js/script.js"></script>
				<!-- End WOWSlider.com BODY section -->
			</div>
			<div class="block">
				<h2 class="title-block"><img src="catalog/view/theme/prom_trans/images/ico-title-01.png" alt="">акции от нашей компании</h2>
				<ul class="stock-list">
					<li>
						<a href="#" class="title"><span class="img"><img src="catalog/view/theme/prom_trans/images/ico-03.png" alt=""></span><span class="text"><span>АКЦИЯ НА ЗАПАСНЫЕ ЧАСТИ</span></span></a>
						<div class="holder">
							<a href="#" class="img"><img src="catalog/view/theme/prom_trans/images/img-06.png" alt=""></a>
							<div class="holder-after">
								<p>При покупке з/ч и последующей их установке на транспортное средство в нашей компании предоставляется скидка в размере 5 % на покупку этих запасных частей.</p>
							</div>
						</div>
					</li>
					<li>
						<a href="#" class="title"><span class="img"><img src="catalog/view/theme/prom_trans/images/ico-04.png" alt=""></span><span class="text"><span>АКЦИЯ НА ДИАГНОСТИКУ ПЕРЕДНЕЙ ПОДВЕСКИ</span></span></a>
						<div class="holder">
							<a href="#" class="img"><img src="catalog/view/theme/prom_trans/images/img-05.png" alt=""></a>
							<div class="holder-after">
								<p>При обслуживании и ремонте автомобиля на сумму более 1000 руб.<br />
предоставляется возможность получить бесплатную проверку передней подвески автомобиля.
</p>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="holder-after">
				<div class="block partners-box">
					<h2 class="title-block"><img src="catalog/view/theme/prom_trans/images/ico-title-03.png" alt="">Партнеры</h2>
					<div class="partners-inner">
						<ul class="partners-list">
							<li>
								<img src="catalog/view/theme/prom_trans/images/logo-01.png" alt="">
								<p>Обслуживание и ремонт, оригинальные запчасти.<br />
SAF. SAF-Holland  является одним из ведущих производителей осей и пневмоподвесок прицепов и полуприцевом в Европе</p>
							</li>
							<li>
								<img src="catalog/view/theme/prom_trans/images/logo-02.png" alt="">
								<p>Обслуживание и ремонт, <br />
оригинальные запчасти</p>
							</li>
							<li>
								<img src="catalog/view/theme/prom_trans/images/logo-03.png" alt="">
								<p>Обслуживание и ремонт, <br />
оригинальные запчасти</p>
							</li>
							<li>
								<img src="catalog/view/theme/prom_trans/images/logo-04.png" alt="">
								<p>Обслуживание и ремонт, <br />
оригинальные запчасти</p>
							</li>
						</ul>
						<a href="#" class="more">Все партнеры</a>
					</div>
				</div>
				<div class="block holder">
					<h2 class="title-block"><img src="catalog/view/theme/prom_trans/images/ico-title-02.png" alt="">Последние новости, акции, скидки</h2>
					<div class="news-box">
						<ul class="news-list">
							<li>
								<a href="#" class="title">ЧЕТЫРЕХЛЕТНЯЯ СОФИ И 18-ТОННЫЙ ГРУЗОВИК</a>
								<div class="holder">
									<a href="#" class="img"><img src="catalog/view/theme/prom_trans/images/img-07.png" alt=""></a>
									<div class="holder">
										<em class="date">05.10.2015</em>
										<p>Непредсказуемый ребенок — и радиоуправляемый 18-тонный грузовой автомобиль. В новом видеоролике VolvoTrucks «Lookwho’sdriving».</p>
										<div class="more-holder">
											<a href="#" class="more">Подробнее</a>
										</div>
									</div>
								</div>
							</li>
							<li>
								<a href="#" class="title">VolvoExchangeProgram в России: видеофильм <br />с мнениями клиентов</a>
								<div class="holder">
									<a href="#" class="img"><img src="catalog/view/theme/prom_trans/images/img-08.png" alt=""></a>
									<div class="holder">
										<em class="date">05.10.2015</em>
										<p>Предлагаем вашему вниманию ролик по программе VolvoExchange, снятый с участием российских клиентов.</p>
										<div class="more-holder">
											<a href="#" class="more">Подробнее</a>
										</div>
									</div>
								</div>
							</li>
							<li>
								<a href="#" class="title">ПУСТЬ ВАШ VOLVO ОСТАНЕТСЯ VOLVO</a>
								<div class="holder">
									<a href="#" class="img"><img src="catalog/view/theme/prom_trans/images/img-09.png" alt=""></a>
									<div class="holder">
										<em class="date">05.10.2015</em>
										<p>Уважаемые клиенты! Представляем вам VolvoExchangeProgram - уникальный способ сохранения надежности, долговечности и безотказности.</p>
										<div class="more-holder">
											<a href="#" class="more">Подробнее</a>
										</div>
									</div>
								</div>
							</li>
						</ul>
						<div class="block map-box">
							<h2 class="title-block"><img src="catalog/view/theme/prom_trans/images/ico-title-04.png" alt="">Как нас найти</h2>
							<div class="contat-box">
								<address>г. Великий Новгород, <br />Ул. Магистральная 11/13</address>
								<span class="phone-contact">8 8162 96 30 30 </span>
								<span class="mail1"><a href="mailto:grk@nbits.ru">grk@nbits.ru</a> </span>
								<span class="mail2"><a href="mailto:parts@nbits.ru">parts@nbits.ru</a> </span>
								<a href="#" class="reg">Запись на ремонт</a>
							</div>
							<div class="map"><img src="catalog/view/theme/prom_trans/images/map.png" width="247" alt=""></div>
						</div>
					</div>
				</div>
			</div>
			<div class="block">
				<h2 class="title-block"><img src="catalog/view/theme/prom_trans/images/ico-title-05.png" alt="">О компании</h2>
				<div class="about-box">
					<p>Компания «<strong>Промтранс</strong>» образована в 1993 году.</p>
					<p><strong>Основное направление деятельности компании</strong> — предоставление квалифицированных услуг технического и сервисного обслуживания грузового автотранспорта, реализация запасных частей и сопутствующих товаров. </p>
					<p>Материально-техническая база ООО «<strong>Промтранс</strong>» состоит из надежного, отвечающего современным требованиям диагностического и сервисного оборудования таких известных производителей, как <strong>Hofmann, Josam, MAHA</strong>. </p>
					<p>Персонал компании постоянно проходит обучение в учебных центрах.</p>
					<ul class="about-info">
						<li>
							<img src="catalog/view/theme/prom_trans/images/ico-05.png" alt="">
							<p>В 2004 году компания «<strong>Промтранс</strong>» получила статус сервисной станции <br />
<strong>RenaultTrucks</strong>;</p>
						</li>
						<li>
							<img src="catalog/view/theme/prom_trans/images/ico-06.png" alt="">
							<p>В 2007 году — статус сервисного партнера <strong>SCHMITZCARGOBULL;</strong></p>
						</li>
						<li>
							<img src="catalog/view/theme/prom_trans/images/ico-07.png" alt="">
							<p>В 2014 года ООО «<strong>Промтранс</strong>» — официальный дилер грузовой техники <strong>VolvoTrucks, RenaultTrucks</strong>.</p>
						</li>
					</ul>
					<p>Наша компания является официальным сервисным партнером брендов <strong>Wabco, Knorr-Bremse, Haldex</strong>.</p>
					<p>Мы специализируемся на техническом обслуживании, гарантийном и послегарантийном ремонте грузовой/прицепной техники.</p>
					<div class="holder"><a href="#" class="more">Подробнее о компании</a></div>
				</div>
			</div>
		</div>