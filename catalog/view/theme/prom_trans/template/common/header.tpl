<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>

  <meta charset="utf-8">
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
  <meta name="viewport" content="width=1000px">
  <meta name="HandheldFriendly" content="true"/>
  <title>Пром Транс</title>

  <link rel="shortcut icon" href="catalog/view/theme/prom_trans/images/favicon.png" type="image/x-icon">
  <link media="all" rel="stylesheet" type="text/css" href="catalog/view/theme/prom_trans/stylesheet/all.css" />
  <!--[if IE]>
    <link media="all" rel="stylesheet" type="text/css" href="css/ie.css" />
  <![endif]-->
  <link media="all" rel="stylesheet" type="text/css" href="catalog/view/theme/prom_trans/stylesheet/wowslider.css" />
  <script type="text/javascript" src="catalog/view/theme/prom_trans/js/clear-form.js"></script>
  <!-- jQuery 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
  <script type="text/javascript" src="catalog/view/theme/prom_trans/js/jquery-1.11.2.js"></script>
  <script type="text/javascript" src="catalog/view/theme/prom_trans/js/main.js"></script>
  <script type="text/javascript" src="catalog/view/theme/prom_trans/js/valign.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('.valign').vAlign();
  });
  </script>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->



<!-- <meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
<!-- <title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?> -->
</head>
<body class="no-res">
  <div class="block popup">
    <h2 class="title-block">Записаться на ремонт/консультацию</h2>
    <form action="#" class="order-popup">
      <fieldset>
        <input type="text" value="Имя (компания)">
        <input type="text" value="Адрес электронной почты (E-mail)">
        <input type="text" value="Телефон контактного лица (+7 816 222 22 22)">
        <input type="text" value="Марка, модель автомобиля или прицепа">
        <input type="text" value="Год выпуска транспортного средства">
        <input type="text" value="VIN номер автомобиля или прицепа">
        <input type="text" value="Желаемое время посещения СТО">
        <input type="text" value="Необходимые работы по ремонту или обслуживанию">
        <span class="warning">*Все поля являются обязательными для заполнения</span>
        <input type="submit" value="Записаться">
      </fieldset>
    </form>
  </div>
  <div id="wrapper">
    <div id="header">
      <div class="inner">
        <a href="http://www.renault-trucks.ru/" target="_blank" class="ico ico-left"><img src="catalog/view/theme/prom_trans/images/ico-01.png" alt=""></a>
        <a href="http://www.volvotrucks.com/trucks/russia-market/ru-ru/Pages/home.aspx" target="_blank" class="ico ico-right"><img src="catalog/view/theme/prom_trans/images/ico-02.png" alt=""></a>
        <div class="header-inner holder-after">
          <span class="phone">
            <span>8 8162 96 30 30</span>
            <a href="#">Свяжитесь с нами</a>
          </span>
          <span class="mail">
            <span><img src="catalog/view/theme/prom_trans/images/ico-mail-01.png" alt=""><a href="mailto:grk@nbits.ru">grk@nbits.ru</a></span>
            <span><img src="catalog/view/theme/prom_trans/images/ico-mail-02.png" alt=""><a href="mailto:parts@nbits.ru">parts@nbits.ru</a></span>
          </span>
          <strong class="logo"><a href="#"></a></strong>
        </div>
        <a href="#" class="menu-phone">
          <span></span>
          <span></span>
          <span></span>
        </a>
        <strong class="logo-mob"><a href="#"><img src="catalog/view/theme/prom_trans/images/logo.png" alt=""></a></strong>
      </div>
    </div>
    <div id="main">
      <div class="menu">
        <ul id="nav">
          <li>
            <a href="/">
              <img src="catalog/view/theme/prom_trans/images/ico-nav-01.png" class="none" alt="">
              <img src="catalog/view/theme/prom_trans/images/ico-nav-01-hover.png" class="hover" alt="">
              <img src="catalog/view/theme/prom_trans/images/ico-nav-01-active.png" class="active" alt="">
            </a>
          </li>
          <li>
            <!-- <a href="#"> -->
            <a href="/index.php?route=information/information&information_id=14">
              <span class="ico">
                <img src="catalog/view/theme/prom_trans/images/ico-nav-02.png" class="none" alt="">
                <img src="catalog/view/theme/prom_trans/images/ico-nav-02-hover.png" class="hover" alt="">
                <img src="catalog/view/theme/prom_trans/images/ico-nav-02-active.png" class="active" alt="">
              </span>
              <span class="item">О компании</span>
            </a>
          </li>
          <li>
            <a href="/index.php?route=information/information&information_id=11">
              <span class="ico">
                <img src="catalog/view/theme/prom_trans/images/ico-nav-03.png" class="none" alt="">
                <img src="catalog/view/theme/prom_trans/images/ico-nav-03-hover.png" class="hover" alt="">
                <img src="catalog/view/theme/prom_trans/images/ico-nav-03-active.png" class="active" alt="">
              </span>
              <span class="item">Услуги</span>
            </a>
          </li>
          <li class="drop">
            <a href="javascript:void(0)" >
              <span class="ico">
                <img src="catalog/view/theme/prom_trans/images/ico-nav-04.png" class="none" alt="">
                <img src="catalog/view/theme/prom_trans/images/ico-nav-04-hover.png" class="hover" alt="">
                <img src="catalog/view/theme/prom_trans/images/ico-nav-04-active.png" class="active" alt="">
              </span>
              <span class="item">Запасные части</span>
            </a>
            <ul class="drop-menu">
              <li>
                <a href="/index.php?route=information/information&information_id=9">Восстановленные запасные части</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="/index.php?route=information/information&information_id=15">
              <span class="ico">
                <img src="catalog/view/theme/prom_trans/images/ico-nav-05.png" class="none" alt="">
                <img src="catalog/view/theme/prom_trans/images/ico-nav-05-hover.png" class="hover" alt="">
                <img src="catalog/view/theme/prom_trans/images/ico-nav-05-active.png" class="active" alt="">
              </span>
              <span class="item">Партнеры</span>
            </a>
          </li>
          <li class="drop">
            <a href="/index.php?route=information/information&information_id=13">
              <span class="ico">
                <img src="catalog/view/theme/prom_trans/images/ico-nav-06.png" class="none" alt="">
                <img src="catalog/view/theme/prom_trans/images/ico-nav-06-hover.png" class="hover" alt="">
                <img src="catalog/view/theme/prom_trans/images/ico-nav-06-active.png" class="active" alt="">
              </span>
              <span class="item">Сервис</span>
            </a>
            <ul class="drop-menu">
              <li>
                <a href="/index.php?route=information/information&information_id=10">Плановое техническое обслуживание Volvo</a>
              </li>
              <li>
                <a href="/index.php?route=information/information&information_id=7">Служба экстренной помощи VAS</a>
              </li>
            </ul>
          </li>
          <li class="drop">
            <a href="javascript:void(0)" >
              <span class="ico">
                <img src="catalog/view/theme/prom_trans/images/ico-nav-07.png" class="none" alt="">
                <img src="catalog/view/theme/prom_trans/images/ico-nav-07-hover.png" class="hover" alt="">
                <img src="catalog/view/theme/prom_trans/images/ico-nav-07-active.png" class="active" alt="">
              </span>
              <span class="item">Новости</span>
            </a>
            <ul class="drop-menu">
              <li>
                <a href="#">Новости нашей компании</a>
              </li>
              <li>
                <a href="/index.php?route=information/information&information_id=16">Новости Volvo</a>
              </li>
            </ul>
          </li>
          <li class="drop">
            <a href="javascript:void(0)" >
              <span class="ico">
                <img src="catalog/view/theme/prom_trans/images/ico-nav-08.png" class="none" alt="">
                <img src="catalog/view/theme/prom_trans/images/ico-nav-08-hover.png" class="hover" alt="">
                <img src="catalog/view/theme/prom_trans/images/ico-nav-08-active.png" class="active" alt="">
              </span>
              <span class="item">Тахография</span>
            </a>
            <ul class="drop-menu">
              <li>
                <a href="/index.php?route=information/information&information_id=17">Установка тахографов</a>
              </li>
              <li>
                <a href="#">Карты водителя и предприятия</a>
              </li>
              <li>
                <a href="/index.php?route=information/information&information_id=12">Документы</a>
              </li>
              <li>
                <a href="#">Прайслист</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="/index.php?route=information/information&information_id=8">
              <span class="ico">
                <img src="catalog/view/theme/prom_trans/images/ico-nav-09.png" class="none" alt="">
                <img src="catalog/view/theme/prom_trans/images/ico-nav-09-hover.png" class="hover" alt="">
                <img src="catalog/view/theme/prom_trans/images/ico-nav-09-active.png" class="active" alt="">
              </span>
              <span class="item">Контакты</span>
            </a>
          </li>
        </ul>
      </div>




<!-- 
<body class="<?php echo $class; ?>">
<nav id="top">
  <div class="container">
    <?php echo $currency; ?>
    <?php echo $language; ?>
    <div id="top-links" class="nav pull-right">
      <ul class="list-inline">
        <li><a href="<?php echo $contact; ?>"><i class="fa fa-phone"></i></a> <span class="hidden-xs hidden-sm hidden-md"><?php echo $telephone; ?></span></li>
        <li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a>
          <ul class="dropdown-menu dropdown-menu-right">
            <?php if ($logged) { ?>
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
            <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
            <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
            <?php } else { ?>
            <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
            <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
            <?php } ?>
          </ul>
        </li>
        <li><a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>"><i class="fa fa-heart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_wishlist; ?></span></a></li>
        <li><a href="<?php echo $shopping_cart; ?>" title="<?php echo $text_shopping_cart; ?>"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_shopping_cart; ?></span></a></li>
        <li><a href="<?php echo $checkout; ?>" title="<?php echo $text_checkout; ?>"><i class="fa fa-share"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_checkout; ?></span></a></li>
      </ul>
    </div>
  </div>
</nav>
<header>
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <div id="logo">
          <?php if ($logo) { ?>
          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
          <?php } else { ?>
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
        </div>
      </div>
      <div class="col-sm-5"><?php echo $search; ?>
      </div>
      <div class="col-sm-3"><?php echo $cart; ?></div>
    </div>
  </div>
</header>
<?php if ($categories) { ?>
<div class="container">
  <nav id="menu" class="navbar">
    <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
      <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav">
        <?php foreach ($categories as $category) { ?>
        <?php if ($category['children']) { ?>
        <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
          <div class="dropdown-menu">
            <div class="dropdown-inner">
              <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
              <ul class="list-unstyled">
                <?php foreach ($children as $child) { ?>
                <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </div>
            <a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a> </div>
        </li>
        <?php } else { ?>
        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
        <?php } ?>
        <?php } ?>
      </ul>
    </div>
  </nav>
</div> -->
<!-- <?php } ?> -->
